#!/bin/bash

samtools index SRR12486971_A.algerae_sorted.bam
samtools index SRR12486972_A.flavus_sorted.bam
samtools index SRR12486974_C.albicans_sorted.bam
samtools index SRR12486978_M.chelonae_sorted.bam
samtools index SRR12486983_HSV1_sorted.bam
samtools index SRR12486988_Alug_sorted.bam
samtools index SRR12486989_Sagalactiae_sorted.bam
samtools index SRR12486990_Saureus_sorted.bam

